from tkinter import *
from tkinter import messagebox
from classSinhvien import sinhVien

def dangnhap():
    tdn=user.get()
    mk=pwd.get()
    
    if tdn=="admin":
        if mk=="123":
            messagebox.showinfo("Thông báo", "Đăng nhập thành công!")
            tk.title("Chương trình quản lý sinh viên.")
            
            lbl1=Label(tk,text=("Thông tin sinh viên:"),font=("Times New Roman",16),fg="Red")
            lbl1.pack()
            
            lbl2=Label(tk,text="Họ tên:",font=("Times New Roman",14))
            lbl2.pack()
            
            name=Entry(tk,width=40,font=("Times New Roman",14))
            name.pack()
            
            lbl3=Label(tk,text="Mã sinh viên:",font=("Times New Roman",14))
            lbl3.pack()
            
            number=Entry(tk,width=40,font=("Times New Roman",14))
            number.pack()
            
            lbl4=Label(tk,text="Điểm lý thuyết:",font=("Times New Roman",14))
            lbl4.pack()
            
            score1=Entry(tk,width=20,font=("Times New Roman",14))
            score1.pack()
            
            lbl5=Label(tk,text="Điểm thực hành:",font=("Times New Roman",14))
            lbl5.pack()
            
            score2=Entry(tk,width=20,font=("Times New Roman",14))
            score2.pack()
            
            lbl6=Label(tk,text="Điểm trung bình:",font=("Times New Roman",14))
            lbl6.pack()
            
            score3=Entry(tk,width=20,font=("Times New Roman",14))
            score3.pack()
            
            def nhap():
                ten=name.get()
                msv=number.get()
                dlt=score1.get()
                dth=score2.get()
                if ten=='':
                    messagebox.showerror("Cảnh báo", "Chưa nhập tên!")
                if msv=='':
                    messagebox.showerror("Cảnh báo", "Chưa nhập mã sinh viên!")
                if dlt=='':
                    messagebox.showerror("Cảnh báo", "Chưa nhập điểm lý thuyết!")
                if dth=='':
                    messagebox.showerror("Cảnh báo", "Chưa nhập điểm thực hành!")
                else:
                    lbl=Label(tk,text=(ten,'-',msv,'-',dlt,'-',dth))
                    lbl.pack()
                    
            btn1=Button(tk,text="Thêm",width=20,font=("Times New Roman",14),command=nhap)
            btn1.pack()
            
            btn2=Button(tk,text="Thoát",width=20,font=("Times New Roman",14))
            btn2.pack()
            
        else:
            messagebox.showerror("Cảnh báo","Đăng nhập thất bại!")
    else:
        messagebox.showerror("Cảnh báo", "Đăng nhập thất bại!")

tk=Tk()
tk.title("Tài khoản")

lbl=Label(tk,text="Tên đăng nhập",font=("Times New Roman",16))
lbl.pack()

user=Entry(tk,width=40)
user.pack()

lbl7=Label(tk,text="Mật khẩu",font=("Times New Roman",16))
lbl7.pack()

pwd=Entry(tk,width=40,show="*")
pwd.pack()

btn=Button(tk,text="Đăng nhập",font=("Times New Roman",14),command=dangnhap)
btn.pack()

tk.mainloop()