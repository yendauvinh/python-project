from tkinter import *
from tkinter import messagebox
import time
import random

class ball():
    def __init__(self,canvas,color,thanh):
        self.thanhtruot=thanh
        self.canvas=canvas
        self.id=canvas.create_oval(10,10,25,25,fill=color)
        self.canvas.move(self.id,50,50)
        start=[1,28,64,7,99,-3,-5.-12]
        random.shuffle(start)
        self.x=start[2]
        self.y=2
        self.over=False
        self.score=0
    
    def vacham(self,pos):
        pos_thanhtruot=self.canvas.coords(self.thanhtruot.id)
        if pos[0]>=pos_thanhtruot[0] and pos[0]<=pos_thanhtruot[2]:
            if pos[1]>=pos_thanhtruot[1] and pos[3]<=pos_thanhtruot[3]:
                self.score+=1    
                return True
        return False
    def run(self):
        self.canvas.move(self.id,self.x,self.y)
        pos=self.canvas.coords(self.id)
        if pos[1]<=0:
            self.y=2
        if pos[3]>=500:
            self.over=True
        if self.vacham(pos)==True:
            self.y=-2
        if pos[0]<=0:
            self.x=2
        if pos[2]>=400:
            self.x=-2
class thanhtruot:
    def __init__(self,canvas,color):
        self.canvas=canvas
        self.id=canvas.create_rectangle(0,0,100,20,fill=color)
        self.canvas.move(self.id,200,300)
        self.canvas.bind_all('<KeyPress-Left>',self.trai)
        self.canvas.bind_all('<KeyPress-Right>',self.phai)
        self.x=0
        self.y=0
    def draw(self):
        self.canvas.move(self.id,self.x,self.y)
    def trai(self,event):
        self.x=-2
    def phai(self,event):
        self.x=2

tk=Tk()
tk.title("Keep your ball *_*")
tk.resizable(0, 0)
cas=Canvas(tk,width=400,height=500)
cas.pack()

thanh=thanhtruot(cas,'red')
bong=ball(cas,'blue',thanh)

while 1:
    if bong.over==False:
        bong.run()
        thanh.draw()
        tk.update_idletasks()
        tk.update()
        time.sleep(0.01)
    else:
        messagebox.showinfo("Game over", "Oc cho")
        break