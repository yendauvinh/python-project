from tkinter import *
from tkinter import messagebox
from classSinhvien import sinhVien

def taikhoan():
    tendangnhap=user1.get()
    matkhau=pwd.get()
    if tendangnhap=="admin":
        if matkhau=="123":
            messagebox.showinfo("Thông báo", "Đăng nhập thành công!")
        else:
            messagebox.showerror("Cảnh báo", "Đăng nhập thất bại!")
    else:
        messagebox.showerror("Cảnh báo","Đăng nhập thất bại!")

def nhap():
    name=ten.get()
    msv=masinhvien.get()
    dlt=diemlythuyet.get()
    dth=diemthuchanh.get()
    if name=='':
        messagebox.showerror("Cảnh báo", "Chưa nhập tên!")
    if msv=='':
        messagebox.showerror("Cảnh báo", "Chưa nhập mã sinh viên!")
    if dlt=='':
        messagebox.showerror("Cảnh báo", "Chưa nhập điểm lý thuyết!")
    if dth=='':
        messagebox.showerror("Cảnh báo", "Chưa nhập điểm thực hành!")
    else:
        lbl=Label(tk,text=(name,'-',msv,'-',dlt,'-',dth))
        lbl.pack()

tk=Tk()

tk.title("Đăng nhập")
lbl5=Label(tk,text="Tài khoản:")
lbl5.pack()

user1=Entry(tk,width=30)
user1.pack()

lbl6=Label(tk,text="Mật khẩu: ")
lbl6.pack()

pwd=Entry(tk,width=30)
pwd.pack()

btn1=Button(tk,text="Đăng nhập",command=taikhoan)
btn1.pack()

tk.title("Chương trình quản lý sinh viên.")
lbl1=Label(tk,text="Họ tên")
lbl1.pack()
ten=Entry(tk,width=40)
ten.pack()

lbl2=Label(tk,text="Mã sinh viên")
lbl2.pack()
masinhvien=Entry(tk,width=40)
masinhvien.pack()

lbl3=Label(tk,text="Điểm lý thuyết")
lbl3.pack()
diemlythuyet=Entry(tk,width=40)
diemlythuyet.pack()

lbl4=Label(tk,text="Điểm thực hành")
lbl4.pack()
diemthuchanh=Entry(tk,width=40)
diemthuchanh.pack()

btn=Button(tk,text="Nhập",command=nhap)
btn.pack()

tk.mainloop()